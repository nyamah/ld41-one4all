﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitAttributes
{
    public float MaxHp;
    public float CurrentHp;
    public float StartingHp;

    public float MovementSpeed;
    public float ShootingSpeed;
    public float DamagePerHit;
}

[Serializable]
public class UnitSpawnConfig
{
    public GameObject Prefab;
    public Team Team;
    public Color AlbedoColor;
    public Color EmissionColor;
}

public enum UnitLifespan
{
    PERMANENT,
    PLANNING,
}

public class Unit : MonoBehaviour
{
    [SerializeField]
    protected UnitAttributes _attributes;

    [SerializeField]
    Renderer _renderer;

    [SerializeField]
    GameObject _projectilePrefab;

    public event Action<Unit, Unit> HitReceived;

    public int UnitId { get; protected set; }
    public float CurrentHp { get { return _attributes.CurrentHp; } protected set { _attributes.CurrentHp = value; } }
    public float MaxHp { get { return _attributes.MaxHp; } protected set { _attributes.MaxHp = value; } }
    public float DamagePerHit { get { return _attributes.DamagePerHit; } }
    public float MovementSpeed {  get { return _attributes.MovementSpeed; } }
    public Team Team { get; protected set; }

    public int CurrentTileX;
    public int CurrentTileY;
    public int CurrentOrientationX;
    public int CurrentOrientationY;

    int _phaseStartTileX, _phaseStartTileY;
    int _phaseStartOrientationX, _phaseStartOrientationY;

    Queue<UnitAction> _unitActionQueue = new Queue<UnitAction>();
    List<Projectile> _projectiles = new List<Projectile>();

    UnitLifespan _unitLifespan;
    float _timeSinceLastActionExecuted;
    float _minTimeBetweenActions = 0.5f;

    public virtual void Init(int unitId, int tileX, int tileY, UnitSpawnConfig cfg, UnitLifespan lifespan = UnitLifespan.PERMANENT)
    {
        UnitId = unitId;

        if (cfg != null)
        {
            Team = cfg.Team;

            if (_renderer != null)
            {
                _renderer.material.SetColor("_Color", cfg.AlbedoColor);
                _renderer.material.SetColor("_Emission", cfg.EmissionColor);
            }
        }

        CurrentTileX = tileX;
        CurrentTileY = tileY;
        CurrentOrientationX = Team == Team.ONE ? 1 : -1;
        CurrentOrientationY = 0;

        _unitLifespan = lifespan;
    }

    public bool IsAlive()
    {
        return CurrentHp > 0f;
    }

    public void EnqueueAction(UnitAction action)
    {
        _unitActionQueue.Enqueue(action);
    }

    public void ReceiveDamage(Unit attacker)
    {
        CurrentHp = Math.Max(0f, CurrentHp - attacker.DamagePerHit);

        if(HitReceived != null)
        {
            HitReceived(this, attacker);
        }
    }

    public bool IsBusy()
    {
        return _unitActionQueue.Count > 0 /*|| _timeSinceLastActionExecuted < _minTimeBetweenActions*/;
    }

    public virtual void OnDead()
    {
        if(_renderer != null)
        {
            _renderer.material.color = new Color(0f, 0f, 0f, 0f);
        }
    }

    public void OnPlanningPhaseStarted()
    {
        _timeSinceLastActionExecuted = _minTimeBetweenActions;

        _phaseStartTileX = CurrentTileX;
        _phaseStartTileY = CurrentTileY;
        _phaseStartOrientationX = CurrentOrientationX;
        _phaseStartOrientationY = CurrentOrientationY;

        _unitActionQueue.Clear();
    }

    public void SetPlanningPhaseStartingPosition(BattleGrid mapGrid)
    {
        SetTilePosition(_phaseStartTileX, _phaseStartTileY, mapGrid);
        SetOrientation(_phaseStartOrientationX, _phaseStartOrientationY);
    }

    public void SetTilePosition(int tileX, int tileY, BattleGrid grid)
    {
        CurrentTileX = tileX;
        CurrentTileY = tileY;
        transform.position = grid.GetTilePosition(tileX, tileY);
    }

    public void SetOrientation(int orientationX, int orientationY)
    {
        float angle;
        if(orientationX != 0)
        {
            angle = orientationX == 1 ? 90f : 270f;
        }
        else
        {
            angle = orientationY == 1 ? 0f : 180f;
        }

        CurrentOrientationX = orientationX;
        CurrentOrientationY = orientationY;
        transform.rotation = Quaternion.Euler(0f, angle, 0f);
    }

    public virtual void ExecuteCurrentAction(float dt, BattleGrid grid, UnitManager unitManager, TurnManager turnManager)
    {
        var currentAction = _unitActionQueue.Count == 0 ? null : _unitActionQueue.Peek();
        if(currentAction != null && currentAction.Execute(dt, this, grid, unitManager, turnManager))
        {
            _timeSinceLastActionExecuted = 0f;
            _unitActionQueue.Dequeue();
        }
    }

    public void InstantiateProjectile(int tileX, int tileY, int directionX, int directionY, BattleGrid grid, UnitManager unitManager, TurnManager turnManager)
    {
        var projectile = Instantiate(_projectilePrefab, grid.GetTilePosition(tileX, tileY, true), Quaternion.identity).GetComponent<Projectile>();
        var direction = (grid.GetTilePosition(tileX + directionX, tileY + directionY) - grid.GetTilePosition(tileX, tileY)).normalized;
        var lifespan = turnManager.CurrentState == TurnManager.TurnState.PLANNING ? UnitLifespan.PLANNING : UnitLifespan.PERMANENT;
        projectile.Init(unitManager.GetNewUnitId(Team), tileX, tileY, Team, _attributes.ShootingSpeed, _attributes.DamagePerHit, direction, unitManager.AllUnits, lifespan);
        unitManager.AllUnits.Add(projectile.UnitId, projectile);
    }

    public void ResetActionQueue()
    {
        _unitActionQueue.Clear();
    }

    public virtual void ProgressUpdate(float dt, BattleGrid grid, UnitManager unitManager)
    {
        _timeSinceLastActionExecuted += dt;
    }

    public virtual void PlanningUpdate(float dt, BattleGrid grid, UnitManager unitManager)
    {
        _timeSinceLastActionExecuted += dt;
    }
}
