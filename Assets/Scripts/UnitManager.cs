﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team : byte
{
    ONE = 0,
    TWO,
    TEAM_NUMBER
}

static class TeamExtensions
{
    public static Team Opposite(this Team team)
    {
        return team == Team.ONE ? Team.TWO : Team.ONE;
    }
}

public class UnitManager : MonoBehaviour
{
    [SerializeField]
    RubySpawnConfig[] _rubySpawnConfigs;

    [SerializeField]
    List<DefenderSpawnConfig> _team1DefenderConfigs;

    [SerializeField]
    List<DefenderSpawnConfig> _team2DefenderConfigs;

    List<DefenderSpawnConfig>[] _teamDefenderConfigs;

    Dictionary<int, Unit> _allUnits = new Dictionary<int, Unit>();
    RubyUnit[] _teamRuby;
    List<DefenderUnit>[] _teamDefenders;
    int[] _teamsNextUnitId;

    public Dictionary<int, Unit> AllUnits { get { return _allUnits; } }

    public void Init(BattleGrid mapGrid)
    {
        const int teamNumber = (int)Team.TEAM_NUMBER;
        _teamRuby = new RubyUnit[teamNumber];
        _teamDefenders = new List<DefenderUnit>[teamNumber];
        for (int i = 0; i < teamNumber; ++i)
        {
            _teamDefenders[i] = new List<DefenderUnit>();
        }
        _teamsNextUnitId = new int[teamNumber];
        for(int i = 0; i < _teamsNextUnitId.Length; ++i)
        {
            _teamsNextUnitId[i] = 100 + i * 10000;
        }

        int currentUnitId = 0;
        foreach (var rubySpawnConfig in _rubySpawnConfigs)
        {
            var ruby = SpawnRuby(currentUnitId++, rubySpawnConfig, mapGrid);
            _teamRuby[(int)rubySpawnConfig.Team] = ruby;
            _allUnits.Add(ruby.UnitId, ruby);
        }

        _teamDefenderConfigs = new List<DefenderSpawnConfig>[teamNumber];
        _teamDefenderConfigs[0] = _team1DefenderConfigs;
        _teamDefenderConfigs[1] = _team2DefenderConfigs;
        for (int teamIdx = 0; teamIdx < teamNumber; ++teamIdx)
        {
            var defenderConfigs = _teamDefenderConfigs[teamIdx];
            var teamRuby = _teamRuby[teamIdx];

            for(int i = 0; i < defenderConfigs.Count; ++i)
            {
                var defender = SpawnDefender(currentUnitId++, teamRuby.CurrentTileX, teamRuby.CurrentTileY, defenderConfigs[i], mapGrid);
                _teamDefenders[teamIdx].Add(defender);
                _allUnits.Add(defender.UnitId, defender);
            }
        }

        for (int i = 0; i < teamNumber; ++i)
        {
            _teamRuby[i].SetupDefenders(_teamDefenders[i]);
        }
    }

    public void PlanningUpdate(float dt, BattleGrid mapGrid)
    {
        foreach(var unit in _allUnits.Values)
        {
            unit.PlanningUpdate(dt, mapGrid, this);
        }
    }

    public void ProgressUpdate(float dt, BattleGrid mapGrid)
    {
        var deadUnits = new List<int>();

        foreach (var unit in _allUnits.Values)
        {
            if (unit.IsAlive())
            {
                unit.ProgressUpdate(dt, mapGrid, this);
            }
            else
            {
                deadUnits.Add(unit.UnitId);
            }
        }

        foreach(var deadId in deadUnits)
        {
            var deadUnit = _allUnits[deadId];
            _allUnits.Remove(deadId);

            deadUnit.OnDead();
            Destroy(deadUnit.gameObject);
        }
    }

    public bool AreAllUnitActionsFinished()
    {
        foreach(var unit in _allUnits.Values)
        {
            if(unit.IsBusy())
            {
                return false;
            }
        }
        return true;
    }

    public Unit GetUnit(int unitId)
    {
        if(_allUnits.ContainsKey(unitId))
        {
            return _allUnits[unitId];
        }
        return null;
    }

    public RubyUnit GetTeamRuby(Team team)
    {
        return _teamRuby[(int)team];
    }

    public List<DefenderUnit> GetTeamDefenders(Team team)
    {
        var defenders = _teamDefenders[(int)team];
        for(int i = defenders.Count - 1; i >= 0; --i)
        {
            if(defenders[i] == null)
            {
                defenders.RemoveAt(i);
            }
        }
        return defenders;
    }

    RubyUnit SpawnRuby(int unitId, RubySpawnConfig cfg, BattleGrid mapGrid)
    {
        var ruby = Instantiate(cfg.Prefab, mapGrid.GetTilePosition(cfg.SpawnTileX, cfg.SpawnTileY), Quaternion.identity).GetComponent<RubyUnit>();
        ruby.Init(unitId, cfg);
        return ruby;
    }

    public void OnStartPlanningPhase()
    {
        foreach(var unit in _allUnits.Values)
        {
            unit.OnPlanningPhaseStarted();
        }
    }

    DefenderUnit SpawnDefender(int unitId, int rubyTileX, int rubyTileY, DefenderSpawnConfig cfg, BattleGrid mapGrid)
    {
        int tileX = rubyTileX + cfg.RubyInitialXPositionOffset;
        int tileY = rubyTileY + cfg.RubyInitialYPositionOffset;

        var defender = Instantiate(cfg.Prefab, mapGrid.GetTilePosition(tileX, tileY), Quaternion.identity).GetComponent<DefenderUnit>();
        defender.Init(unitId, tileX, tileY, cfg);
        return defender;
    }

    public int GetNewUnitId(Team team)
    {
        int teamIdx = (int)team;
        int id = _teamsNextUnitId[teamIdx];
        _teamsNextUnitId[teamIdx]++;
        return id;
    }
}
