﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleGrid : MonoBehaviour
{
    [SerializeField]
    int _xSize;

    [SerializeField]
    int _ySize;

    [SerializeField]
    float _xTileSize = 1f;

    [SerializeField]
    float _yTileSize = 1f;

    [SerializeField]
    float _xSpacing;

    [SerializeField]
    float _ySpacing;

    [SerializeField]
    GameObject _tilePrefab;

    [SerializeField]
    float _floorElevation;

    SpriteRenderer[,] _tiles;

    void Awake()
    {
        _tiles = new SpriteRenderer[_ySize, _xSize];

        for (int i = 0; i < _ySize; ++i)
        {
            for(int j = 0; j < _xSize; ++j)
            {
                var tile = Instantiate(_tilePrefab, GetTilePosition(j, i, true), Quaternion.identity);
                tile.transform.parent = transform;
                _tiles[i, j] = tile.GetComponent<SpriteRenderer>();
            }
        }
    }

    public Vector3 GetTilePosition(int x, int y, bool useFloorElevation = false)
    {
        var gridPosition = transform.position;
        float yPosition = gridPosition.y + y * (_ySpacing + _yTileSize);
        float xPosition = gridPosition.x + x * (_xSpacing + _xTileSize);
        float height = useFloorElevation ? _floorElevation : 0f;

        return new Vector3(xPosition, height, yPosition);
    }
}
