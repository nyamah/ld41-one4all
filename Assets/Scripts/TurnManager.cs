﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public enum TurnState
    {
        PLANNING,
        SENDING,
        EXECUTING,
        SYNCING,
    }

    public event Action<TurnState> PhaseFinished;

    [SerializeField]
    float _planningPhaseDuration = 10f;

    public TurnState CurrentState;

    float _currentPlanningTime;

    public float PlanningTimeLeft
    {
        get
        {
            return _planningPhaseDuration - _currentPlanningTime;
        }
    }

    public void Update(float dt)
    {
        if(CurrentState == TurnState.PLANNING)
        {
            _currentPlanningTime += dt;
            if(_currentPlanningTime >= _planningPhaseDuration)
            {
                OnPlanningPhaseFinished();
            }
        }
    }

    public void StartPlanningPhase()
    {
        _currentPlanningTime = 0f;
        CurrentState = TurnState.PLANNING;
    }

    void OnPlanningPhaseFinished()
    {
        if(PhaseFinished != null)
        {
            PhaseFinished(CurrentState);
        }
    }

    public void StartExecutingPhase()
    {
        CurrentState = TurnState.EXECUTING;
    }

    public void StartSyncingPhase()
    {
        CurrentState = TurnState.SYNCING;
    }

    public void StartSendingPhase()
    {
        CurrentState = TurnState.SENDING;
    }

    void OnSyncingPhaseFinished()
    {
        //CurrentState = TurnState.EXECUTING;
    }
}
