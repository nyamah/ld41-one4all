﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DefenderSpawnConfig : UnitSpawnConfig
{
    public int RubyInitialXPositionOffset;
    public int RubyInitialYPositionOffset;
}

public class DefenderUnit : Unit
{
    public void Init(int unitId, int tileX, int tileY, DefenderSpawnConfig cfg)
    {
        base.Init(unitId, tileX, tileY, cfg);
    }

    
}
