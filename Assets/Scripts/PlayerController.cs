﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    GameObject _unitUnderControlMarkPrefab;

    [SerializeField]
    float _forcedDelayBetweenInputs = 1f;

    GameObject _unitUnderControlMark;
    DefenderUnit _unitUnderControl;

    float _currentDelay;

    public void OnUnitUnderControlSelected(DefenderUnit defender)
    {
        _unitUnderControl = defender;

        if (_unitUnderControlMark == null)
        {
            _unitUnderControlMark = Instantiate(_unitUnderControlMarkPrefab);
        }
        _unitUnderControlMark.transform.position = _unitUnderControl.transform.position;
        _unitUnderControlMark.transform.parent = _unitUnderControl.transform;
    }

	public Vector2 GetMovementVector()
    {
        if(_currentDelay < _forcedDelayBetweenInputs)
        {
            return Vector2.zero;
        }

        if(_unitUnderControl == null || _unitUnderControl.IsBusy())
        {
            return Vector2.zero;
        }

        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");

        if(xAxis != 0f)
        {
            _currentDelay = 0f;
            return xAxis > 0f ? Vector2.right : -Vector2.right;
        }
        if(yAxis != 0f)
        {
            _currentDelay = 0f;
            return yAxis > 0f ? Vector2.up : -Vector2.up;
        }

        return Vector2.zero;
    }

    public Vector2 GetFireVector()
    {
        if (_currentDelay < _forcedDelayBetweenInputs)
        {
            return Vector2.zero;
        }

        if (_unitUnderControl == null || _unitUnderControl.IsBusy())
        {
            return Vector2.zero;
        }

        float fire = Input.GetAxis("Fire1");
        if (fire != 0f)
        {
            _currentDelay = 0f;
            return new Vector2(_unitUnderControl.CurrentOrientationX, _unitUnderControl.CurrentOrientationY);
        }

        return Vector2.zero;
    }

    public int GetChangeCharacterInt()
    {
        if(Input.GetKeyDown(KeyCode.Z))
        {
            return -1;
        }
        else if(Input.GetKeyDown(KeyCode.X))
        {
            return 1;
        }

        return 0;
    }

    public void Update()
    {
        _currentDelay += Time.deltaTime;
    }
}
