﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    [SerializeField]
    UnitManager _unitManager;

    [SerializeField]
    TurnManager _turnManager;

    [SerializeField]
    BattleGrid _mapGrid;

    [SerializeField]
    PlayerController _playerController;

    [SerializeField]
    BattleUi _battleUi;

    bool _gameStarted = false;
    bool _gameFinished = false;
    Team _winnerTeam;
    int _connectedPlayers = 0;
    Team _localPlayerTeam;
    TeamActionBuffer _localPlayerActionBuffer;
    TeamActionBuffer[] _teamActionBuffers = new TeamActionBuffer[(int)Team.TEAM_NUMBER];

    List<int> _bannedCharacterIdxs = new List<int>();
    int _currentControlledDefenderIdx = 0;

    private void Start()
    {
        _battleUi = FindObjectOfType<BattleUi>();
        _unitManager.Init(_mapGrid);

        _unitManager.GetTeamRuby(Team.ONE).HitReceived += OnRubyHit;
        _unitManager.GetTeamRuby(Team.TWO).HitReceived += OnRubyHit;
    }

    private void Update()
    {
        float dt = Time.deltaTime;

        if(!_gameStarted)
        {
            _battleUi.UpdateWaitingForOpponent();
            return;
        }

        if(_gameFinished)
        {
            _battleUi.UpdateFinished(_winnerTeam == _localPlayerTeam);
            return;
        }

        _turnManager.Update(dt);
        if (_turnManager.CurrentState == TurnManager.TurnState.PLANNING)
        {
            TryIssuingActionsToSelectedUnit();
            ExecuteUnitActions(_localPlayerTeam);
            _unitManager.PlanningUpdate(dt, _mapGrid);
            _battleUi.UpdatePlanning(_turnManager.PlanningTimeLeft);
        }
        else if(_turnManager.CurrentState == TurnManager.TurnState.SENDING)
        {
            bool finishedSending = true;
            foreach (var teamBuffer in _teamActionBuffers)
            {
                finishedSending = finishedSending && teamBuffer.HasPlanningFinished();
            }

            if(finishedSending)
            {
                foreach (var teamBuffer in _teamActionBuffers)
                {
                    teamBuffer.ResetPlanningFinishedMark();
                }
                StartExecutingPhase();
            }
        }
        else if (_turnManager.CurrentState == TurnManager.TurnState.EXECUTING)
        {
            ExecuteUnitActions(Team.ONE);
            ExecuteUnitActions(Team.TWO);
            _unitManager.ProgressUpdate(dt, _mapGrid);
            _battleUi.UpdateExecuting();

            if (_unitManager.AreAllUnitActionsFinished())
            {
                StartSyncPhase();
            }
        }
        else if (_turnManager.CurrentState == TurnManager.TurnState.SYNCING)
        {
            bool finishedSyncing = true;
            foreach (var teamBuffer in _teamActionBuffers)
            {
                finishedSyncing = finishedSyncing && teamBuffer.HasExecutionFinished();
            }

            if (finishedSyncing)
            {
                foreach (var teamBuffer in _teamActionBuffers)
                {
                    teamBuffer.ResetExecutionFinishedMark();
                }
                StartPlanningPhase();
            }
        }
    }

    public void OnTeamActionBufferConnected(TeamActionBuffer buffer)
    {
        _teamActionBuffers[(int)buffer.Team] = buffer;
        buffer.ActionsReceivedFromServer += OnActionsReceivedFromServer;

        if (buffer.isLocalPlayer)
        {
            _localPlayerActionBuffer = buffer;
            _localPlayerTeam = buffer.Team;
        }

        _connectedPlayers++;
        if(_connectedPlayers >= 2)
        {
            StartGame();
        }
    }

    void StartGame()
    {
        StartPlanningPhase();
        _turnManager.PhaseFinished += OnPhaseFinished;
        _gameStarted = true;
    }

    void OnPhaseFinished(TurnManager.TurnState finishedPhase)
    {
        if(finishedPhase == TurnManager.TurnState.PLANNING)
        {
            StartSendingPhase();
        }
    }

    void StartPlanningPhase()
    {
        _bannedCharacterIdxs.Clear();
        _currentControlledDefenderIdx = 0;

        //_localPlayerActionBuffer.CmdReset();
        foreach (var buffer in _teamActionBuffers)
        {
            buffer.Buffer.Clear();
        }

        _turnManager.StartPlanningPhase();
        _unitManager.OnStartPlanningPhase();
        _playerController.OnUnitUnderControlSelected(_unitManager.GetTeamDefenders(_localPlayerTeam)[_currentControlledDefenderIdx]);
    }

    void StartSyncPhase()
    {
        _localPlayerActionBuffer.CmdSetExecutionFinishedMark();
        _turnManager.StartSyncingPhase();
    }

    void StartSendingPhase()
    {
        _localPlayerActionBuffer.SendBufferedActionsCmd();
        _localPlayerActionBuffer.CmdSetPlanningFinishedMark();
        _turnManager.StartSendingPhase();
    }

    void StartExecutingPhase()
    {
        _turnManager.StartExecutingPhase();

        foreach (var teamActionBuffer in _teamActionBuffers)
        {
            foreach(var unit in _unitManager.GetTeamDefenders(teamActionBuffer.Team))
            {
                unit.ResetActionQueue();
                unit.SetPlanningPhaseStartingPosition(_mapGrid);
                foreach (var action in teamActionBuffer.GetUnitActions(unit.UnitId))
                {
                    unit.EnqueueAction(action);
                }
            }
        }
    }

    void OnActionsReceivedFromServer(TeamActionBuffer buffer)
    {
        var teamUnits = _unitManager.GetTeamDefenders(buffer.Team);
        foreach(var unit in teamUnits)
        {
            unit.ResetActionQueue();
        }

        foreach(var action in buffer.Buffer)
        {
            _unitManager.GetUnit(action.UnitId).EnqueueAction(action);
        }
    }

    void TryIssuingActionsToSelectedUnit()
    {
        if(_currentControlledDefenderIdx == -1)
        {
            return;
        }

        var movement = _playerController.GetMovementVector();
        var fire = _playerController.GetFireVector();
        var switchCharacter = _playerController.GetChangeCharacterInt();
        if (movement.magnitude != 0f)
        {
            var defender = _unitManager.GetTeamDefenders(_localPlayerTeam)[_currentControlledDefenderIdx];
            int tileX = defender.CurrentTileX;
            int tileY = defender.CurrentTileY;
            tileX += (int)movement.x;
            tileY += (int)movement.y;

            var action = _localPlayerActionBuffer.AddMoveAction(defender.UnitId, tileX, tileY);
            defender.EnqueueAction(action);
        }
        else if(fire.magnitude != 0f)
        {
            var defender = _unitManager.GetTeamDefenders(_localPlayerTeam)[_currentControlledDefenderIdx];
            var action = _localPlayerActionBuffer.AddAttackAction(defender.UnitId, (int)fire.x, (int)fire.y);
            defender.EnqueueAction(action);
            _bannedCharacterIdxs.Add(_currentControlledDefenderIdx);
            switchCharacter = 1; // Force character switch
        }

        if (switchCharacter != 0)
        {
            var characters = _unitManager.GetTeamDefenders(_localPlayerTeam);
            if (_bannedCharacterIdxs.Count == characters.Count)
            {
                _currentControlledDefenderIdx = -1; // All units banned
            }
            else
            {
                _currentControlledDefenderIdx += switchCharacter;
                if (_currentControlledDefenderIdx < 0) _currentControlledDefenderIdx += characters.Count;
                _currentControlledDefenderIdx = _currentControlledDefenderIdx % characters.Count;

                while(_bannedCharacterIdxs.Contains(_currentControlledDefenderIdx) || characters[_currentControlledDefenderIdx] == null || !characters[_currentControlledDefenderIdx].IsAlive())
                {
                    _currentControlledDefenderIdx = _currentControlledDefenderIdx + switchCharacter;
                    if (_currentControlledDefenderIdx < 0) _currentControlledDefenderIdx += characters.Count;
                    _currentControlledDefenderIdx = _currentControlledDefenderIdx % characters.Count;
                }

                _playerController.OnUnitUnderControlSelected(characters[_currentControlledDefenderIdx]);
            }
        }
    }

    void ExecuteUnitActions(Team team)
    {
        var defenders = _unitManager.GetTeamDefenders(team);
        foreach(var defender in defenders)
        {
            //if (!defender.IsBusy())
            {
                defender.ExecuteCurrentAction(Time.deltaTime, _mapGrid, _unitManager, _turnManager);
            }
        }
    }

    void OnRubyHit(Unit ruby, Unit attacker)
    {
        if(ruby.CurrentHp <= 0f)
        {
            PlayerWins(ruby.Team.Opposite());
        }
    }

    void PlayerWins(Team winnerTeam)
    {
        Debug.Log("Player " + winnerTeam + " wins!!");
        _winnerTeam = winnerTeam;
        _gameFinished = true;
    }
}
