﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleUi : MonoBehaviour
{
    [SerializeField]
    GameObject _planningUiContainer;

    [SerializeField]
    Text _planningTimerLabel;

    [SerializeField]
    GameObject _executingUiContainer;

    [SerializeField]
    GameObject _youWinUiContainer;

    [SerializeField]
    GameObject _youLoseUiContainer;

    [SerializeField]
    GameObject _waitingForOpponentContainer;

    public void UpdatePlanning(float planningTimeLeft)
    {
        _planningUiContainer.SetActive(true);
        _planningTimerLabel.text = ((int)planningTimeLeft).ToString() + "s";

        _executingUiContainer.SetActive(false);

        _youWinUiContainer.SetActive(false);
        _youLoseUiContainer.SetActive(false);
        _waitingForOpponentContainer.SetActive(false);
    }

    public void UpdateExecuting()
    {
        _planningUiContainer.SetActive(false);
        _executingUiContainer.SetActive(true);
        _youWinUiContainer.SetActive(false);
        _youLoseUiContainer.SetActive(false);
        _waitingForOpponentContainer.SetActive(false);
    }

    public void UpdateFinished(bool youWin)
    {
        _planningUiContainer.SetActive(false);
        _executingUiContainer.SetActive(false);

        _youWinUiContainer.SetActive(youWin);
        _youLoseUiContainer.SetActive(!youWin);
        _waitingForOpponentContainer.SetActive(false);
    }

    internal void UpdateWaitingForOpponent()
    {
        _waitingForOpponentContainer.SetActive(true);

        _planningUiContainer.SetActive(false);
        _executingUiContainer.SetActive(false);

        _youWinUiContainer.SetActive(false);
        _youLoseUiContainer.SetActive(false);
    }
}
