﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : Unit
{
    [SerializeField]
    float _radius = 1f;

    [SerializeField]
    float _lifetime = 5f;
    
    float _speed;
    float _damage;
    Vector3 _direction;
    float _remainingLifetime;
    Dictionary<int, Unit> _allUnits;
    UnitLifespan _lifespan;

    public void Init(int unitId, int tileX, int tileY, Team team, float shootingSpeed, float damage, Vector3 shootingDirection, Dictionary<int, Unit> allUnits, UnitLifespan lifespan)
    {
        base.Init(unitId, tileX, tileY, null, lifespan);

        _attributes.DamagePerHit = damage;
        _attributes.MovementSpeed = shootingSpeed;
        _attributes.MaxHp = _attributes.CurrentHp = _attributes.StartingHp = 1f;

        Team = team;
        _speed = shootingSpeed;
        _direction = shootingDirection;
        _remainingLifetime = _lifetime;
        _allUnits = allUnits;
        _lifespan = lifespan;
    }

    public override void PlanningUpdate(float dt, BattleGrid grid, UnitManager unitManager)
    {
        base.PlanningUpdate(dt, grid, unitManager);

        if (_lifespan == UnitLifespan.PLANNING)
        {
            Advance(dt);
        }
    }

    public override void ProgressUpdate(float dt, BattleGrid grid, UnitManager unitManager)
    {
        base.ProgressUpdate(dt, grid, unitManager);

        if(_lifespan == UnitLifespan.PLANNING)
        {
            SelfDestroy();
            return;
        }

        Advance(dt);

        _remainingLifetime -= dt;
        if (/*_remainingLifetime <= 0f ||*/ Hit())
        {
            SelfDestroy();
        }
    }

    void Advance(float dt)
    {
        transform.position += _direction * _speed * dt;
    }

    bool Hit()
    {
        foreach(var unit in _allUnits.Values)
        {
            if(unit.Team != Team && Collides(unit))
            {
                DoDamage(unit);
                return true;
            }
        }
        return false;
    }

    bool Collides(Unit u)
    {
        if(!u.IsAlive())
        {
            return false;
        }

        var projectilePos = transform.position;
        projectilePos.y = 0f;
        var unitPos = u.transform.position;
        unitPos.y = 0f;
        return Vector3.Distance(projectilePos, unitPos) <= _radius;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, _radius);
    }

    void DoDamage(Unit u)
    {
        u.ReceiveDamage(this);
    }

    void SelfDestroy()
    {
        CurrentHp = 0f;
    }
}
