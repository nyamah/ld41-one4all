﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public abstract class UnitAction
{
    public abstract byte ActionId { get; }
    public int UnitId;

    public abstract void Serialize(NetworkWriter writer);
    public abstract void Deserialize(NetworkReader reader);
    public abstract bool Execute(float dt, Unit unit, BattleGrid grid, UnitManager unitManager, TurnManager turnManager); // Returns true when action is satisfied
    public abstract void SendServerCommand(TeamActionBuffer buffer);
}

public class MoveUnitAction : UnitAction
{
    public int DestinationTileX;
    public int DestinationTileY;

    public override byte ActionId
    {
        get
        {
            return TeamActionBuffer.MoveActionId;
        }
    }

    public override void Deserialize(NetworkReader reader)
    {
        UnitId = reader.ReadInt32();
        DestinationTileX = reader.ReadInt32();
        DestinationTileY = reader.ReadInt32();
    }

    public override bool Execute(float dt, Unit unit, BattleGrid grid, UnitManager unitManager, TurnManager turnManager)
    {
        var targetPosition = grid.GetTilePosition(DestinationTileX, DestinationTileY);

        unit.transform.position += (targetPosition - unit.transform.position).normalized * unit.MovementSpeed * dt;
        if(Vector3.Distance(unit.transform.position, targetPosition) <= 0.1f)
        {
            int orientationX = GetMovementOrientation(unit.CurrentTileX, DestinationTileX);
            int orientationY = GetMovementOrientation(unit.CurrentTileY, DestinationTileY);

            unit.SetTilePosition(DestinationTileX, DestinationTileY, grid);
            unit.SetOrientation(orientationX, orientationY);
            
            return true;
        }

        return false;
    }

    int GetMovementOrientation(int originalPosition, int targetPosition)
    {
        return targetPosition - originalPosition;
    }

    public override void SendServerCommand(TeamActionBuffer buffer)
    {
        buffer.CmdAddMoveAction(UnitId, DestinationTileX, DestinationTileY);
    }

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(UnitId);
        writer.Write(DestinationTileX);
        writer.Write(DestinationTileY);
    }
}

public class AttackUnitAction : UnitAction
{
    public int ShotDirectionX;
    public int ShotDirectionY;

    float _minimumDistanceWhileExecuting = 2f;

    public override byte ActionId
    {
        get
        {
            return TeamActionBuffer.AttackActionId;
        }
    }

    public override void Deserialize(NetworkReader reader)
    {
        UnitId = reader.ReadInt32();
        ShotDirectionX = reader.ReadInt32();
        ShotDirectionY = reader.ReadInt32();
    }

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(UnitId);
        writer.Write(ShotDirectionX);
        writer.Write(ShotDirectionY);
    }

    public override bool Execute(float dt, Unit unit, BattleGrid grid, UnitManager unitManager, TurnManager turnManager)
    {
        unit.InstantiateProjectile(unit.CurrentTileX + ShotDirectionX, unit.CurrentTileY + ShotDirectionY, ShotDirectionX, ShotDirectionY, grid, unitManager, turnManager);

        return true;
    }

    public override void SendServerCommand(TeamActionBuffer buffer)
    {
        buffer.CmdAddAttackAction(UnitId, ShotDirectionX, ShotDirectionY);
    }
}

public class TeamActionBuffer : NetworkBehaviour
{
    public const byte MoveActionId = 0;

    public const byte AttackActionId = 1;

    public event Action<TeamActionBuffer> ActionsReceivedFromServer;

    public Team Team;
    public List<UnitAction> Buffer = new List<UnitAction>();
    bool _bufferChanges = false;

    private void Start()
    {
        var netId = GetComponent<NetworkIdentity>().netId;
        Team = netId.Value <= 1 ? Team.ONE : Team.TWO;

        var battleManager = FindObjectOfType<BattleManager>();
        battleManager.OnTeamActionBufferConnected(this);
    }

    public List<UnitAction> GetUnitActions(int unitId)
    {
        var res = new List<UnitAction>();
        foreach(var action in Buffer)
        {
            if(action.UnitId == unitId)
            {
                res.Add(action);
            }
        }
        return res;
    }

    [Command]
    public void CmdReset()
    {
        Buffer.Clear();
        _bufferChanges = true;
        SetDirtyBit(1);
    }

    [Command]
    public void CmdAddMoveAction(int unitId, int targetTileX, int targetTileY)
    {
        if(!isLocalPlayer) AddMoveAction(unitId, targetTileX, targetTileY);

        _bufferChanges = true;
        SetDirtyBit(1);
    }

    public UnitAction AddMoveAction(int unitId, int targetTileX, int targetTileY)
    {
        var action = new MoveUnitAction()
        {
            UnitId = unitId,
            DestinationTileX = targetTileX,
            DestinationTileY = targetTileY
        };

        AddActionToBuffer(action);
        return action;
    }
    
    [Command]
    public void CmdAddAttackAction(int unitId, int targetDirX, int targetDirY)
    {
        if (!isLocalPlayer) AddAttackAction(unitId, targetDirX, targetDirY);

        _bufferChanges = true;
        SetDirtyBit(1);
    }

    public UnitAction AddAttackAction(int unitId, int directionX, int directionY)
    {
        var action = new AttackUnitAction()
        {
            UnitId = unitId,
            ShotDirectionX = directionX,
            ShotDirectionY = directionY
        };

        AddActionToBuffer(action);
        return action;
    }

    [Command]
    public void CmdSetExecutionFinishedMark()
    {
        SetExecutionFinishedMark();
        SetDirtyBit(1);
    }

    [Command]
    public void CmdSetPlanningFinishedMark()
    {
        SetPlanningFinishedMark();
        SetDirtyBit(1);
    }

    UnitAction CreateActionInstance(byte type)
    {
        switch(type)
        {
            case MoveActionId:
                return new MoveUnitAction();
            case AttackActionId:
                return new AttackUnitAction();
            default:
                Debug.LogError("Trying to create an unknown action instance: " + type);
                return null;
        }
    }

    public void SendBufferedActionsCmd()
    {
        foreach(var action in Buffer)
        {
            action.SendServerCommand(this);
        }
    }
    
    public override bool OnSerialize(NetworkWriter writer, bool initialState)
    {
        bool dataWritten = base.OnSerialize(writer, initialState);

        var oldBufferChanges = _bufferChanges;
        _bufferChanges = false;

        writer.Write(oldBufferChanges);
        if (oldBufferChanges)
        {
            writer.Write(Buffer.Count);
            for (int i = 0; i < Buffer.Count; ++i)
            {
                writer.Write(Buffer[i].ActionId);
                Buffer[i].Serialize(writer);
            }
        }

        writer.Write(_executionFinishedMark);
        writer.Write(_planningFinishedMark);

        return dataWritten || oldBufferChanges || _executionFinishedMark || _planningFinishedMark;
    }

    public override void OnDeserialize(NetworkReader reader, bool initialState)
    {
        base.OnDeserialize(reader, initialState);

        bool bufferChanges = reader.ReadBoolean();
        if (bufferChanges)
        {
            Buffer.Clear();

            int count = reader.ReadInt32();
            for (int i = 0; i < count; ++i)
            {
                var action = CreateActionInstance(reader.ReadByte());
                action.Deserialize(reader);
                AddActionToBuffer(action);
            }

            if (ActionsReceivedFromServer != null)
            {
                ActionsReceivedFromServer(this);
            }
        }

        _executionFinishedMark = reader.ReadBoolean();
        _planningFinishedMark = reader.ReadBoolean();
    }

    void AddActionToBuffer(UnitAction action)
    {
        Buffer.Add(action);
    }

    bool _executionFinishedMark = false;
    bool _planningFinishedMark = false;
    void SetExecutionFinishedMark()
    {
        _executionFinishedMark = true;
    }

    void SetPlanningFinishedMark()
    {
        _planningFinishedMark = true;
    }

    public bool HasPlanningFinished()
    {
        return _planningFinishedMark;
    }

    public bool HasExecutionFinished()
    {
        return _executionFinishedMark;
    }

    public void ResetExecutionFinishedMark()
    {
        _executionFinishedMark = false;
    }

    public void ResetPlanningFinishedMark()
    {
        _planningFinishedMark = false;
    }

    //private void OnGUI()
    //{
    //    var deb = "ActionBuffer: local? " + isLocalPlayer;
    //    deb += " netid:" + netId.Value + " team: "+Team;
    //    deb += " execFinMark: " + _executionFinishedMark;
    //    deb += " planFinMark: " + _planningFinishedMark;
    //    deb += " action count: " + Buffer.Count;
    //    foreach (var action in Buffer)
    //    {
    //        deb += " type" + action.ActionId;
    //    }
    //    GUILayout.BeginVertical();
    //    if (isLocalPlayer) GUILayout.Label("");
    //    GUILayout.Label(deb);
    //    GUILayout.EndVertical();
    //}
}
