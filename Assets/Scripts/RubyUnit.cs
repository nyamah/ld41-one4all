﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RubySpawnConfig : UnitSpawnConfig
{
    public int SpawnTileX;
    public int SpawnTileY;
}

public class RubyUnit : Unit
{
    List<DefenderUnit> _defenders;

    public void Init(int unitId, RubySpawnConfig cfg)
    {
        base.Init(unitId, cfg.SpawnTileX, cfg.SpawnTileY, cfg);
    }

    public void SetupDefenders(List<DefenderUnit> defenders)
    {
        _defenders = defenders;
    }

    
}
